import app.Utils as utils
import numpy as np


def print_cm( model):
  train_data_npy = np.load(utils.TRAIN_DATA_NPY, encoding="latin1")
  test_data = train_data_npy[-1000:]
  total = 0
  real = 0
  confusion_matrix =  [ [0] * 10 for _ in range(10)]
  for num, data in enumerate(test_data[:]):
    img_num = np.argmax(data[1])
    img_data = data[0]
    orig = img_data
    data = img_data.reshape(224, 224, 1)
    model_out = model.predict([data])[0]
    confusion_matrix[img_num][np.argmax(model_out)] += 1
    if(np.argmax(model_out) == img_num):
      real +=1
    total +=1
  print("correct pics : " , real, "total pics : ",total,"accuracy : ",float(real*100)/total)
  for i in range(10):
    print(confusion_matrix[i])
