import app.DataProcessor as dp
import app.CNN as cnn
import numpy as np
import app.ConfusionMatrix as cm
import app.Tester as Tester

if __name__ == '__main__':
    print("Starting ...................................................................................................")
    dataProcessor = dp.DataProcessor()
    training_data = dataProcessor.create_training_data()
    print("Training Data processed ! ..................................................................................")

    cnn = cnn.ConvolutionalNeuralNetwork()
    cnn_model = cnn.create_model(training_data)
    print("Model trained ! .............................................................................................")

    cm.print_cm(cnn_model)
    print("Confusion Matrix .............................................................................................")



    tester = Tester.Tester(cnn_model)
    tester.test()
